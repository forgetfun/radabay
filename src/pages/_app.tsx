import { cache } from "@emotion/css";
import { CacheProvider, ThemeProvider } from "@emotion/react";
import { AppProps } from "next/app";
import React from "react";
import { globalStyles } from "../config/styles";
import { baseTheme } from "../config/theming/themes/base";

const Providers: React.FC = ({ children }) => (
	<CacheProvider value={cache}>
		<ThemeProvider theme={baseTheme}>{children}</ThemeProvider>
	</CacheProvider>
);

const App: React.FC<AppProps> = ({ Component, pageProps }) => (
	<Providers>
		{globalStyles}
		<Component {...pageProps} />
	</Providers>
);

export default App;
