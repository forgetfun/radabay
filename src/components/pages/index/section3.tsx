import styled from "@emotion/styled";
import React from "react";

const Container = styled.section`
	width: 100%;

	display: flex;
	justify-content: center;
	align-items: flex-end;
	flex-wrap: wrap;

	/* !TODO(theming): move color to theme */
	border-top: 0.5rem solid black;
	border-bottom: 0.5rem solid black;
`;

const Image = styled.img`
	flex: 1;
	min-width: 15rem;
	height: 10rem;

	object-fit: cover;
`;

export const Section3: React.FC = () => (
	<Container>
		<Image src="https://www.romaniajournal.ro/wp-content/uploads/2015/05/children.jpg" />
		<Image src="https://www.creative-kids.net.au/wp-content/uploads/2019/07/raising-happy-children.jpg" />
		<Image src="https://websterunitedmethodist.org/wp-content/uploads/2019/09/Young-Children_Reduced-Cropped.jpg" />
		<Image src="https://www.highlightpr.co.uk/wp-content/uploads/2016/07/iStock_children-with-balloons_HL-blog.jpg" />
	</Container>
);
