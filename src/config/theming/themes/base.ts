import { linearGradient, transparentize } from "polished";
import { Theme } from "..";

const colors = {
	black: "#000",
	white: "#fff",
	orange: "#fb5607",
	red: "#F00040",
	yellow: "#ffbe0b",
	blue: "#0060FA",
	green: "#54D421",
	cyan: "#00E1FF",
};

export const baseTheme: Theme = {
	general: {
		layout: {
			heading: {
				backgroundColor: transparentize(0.4)(colors.white),
				textStyles: linearGradient({
					toDirection: "to right",
					colorStops: [colors.orange, colors.yellow, colors.green, colors.cyan],
				}),
				borderColor: colors.yellow,
			},
		},
		button: {
			textColor: colors.white,
			backgroundColor: colors.blue,
		},
	},
	pages: {
		index: {
			section1: {
				background: colors.red,
			},
			section2: {
				textColor: colors.white,
				background: colors.yellow,
			},
		},
	},
};
