import { css, SerializedStyles } from "@emotion/react";

const font = (family: string, weight: number): SerializedStyles => css`
	font-family: ${family};
	font-weight: ${weight};
`;

export const getFonts = <TWeight extends number>(
	family: string, weights: TWeight[]
): Record<TWeight, SerializedStyles> =>
		weights.reduce(
			(acc, weight) => {
				acc[weight] = font(family, weight);
				return acc;
			},
			{} as Record<TWeight, SerializedStyles>
		);
